<?php
/*
Plugin Name: Membership Leak Tracker
Plugin URI: 
Description: Helps trace any leaked content from a Wordpress site based on the info of a member/user who downloaded the files.
Version: 2.1
Author: QualitynAgility
Author URI: http://codecanyon.net/user/QualitynAgility
*/
if ( !class_exists( 'Membership_Leak_Tracker' )){   
	class Membership_Leak_Tracker{
		private $key = 'lsu739dfgs3';
		private $tmp;
		private $launchpad;
		
		
		function __construct() {
			register_activation_hook( __FILE__, array($this, 'install') );
			register_deactivation_hook( __FILE__, array($this, 'de_activate') );
			add_action( 'admin_menu', array(&$this, 'admin_menu') );
			add_action('wp_enqueue_scripts', array(&$this, 'frontend_scripts') );
			add_action('admin_enqueue_scripts', array(&$this, 'scripts_method') );
			add_shortcode('leaktracker',array(&$this, 'leaktracker'));
			add_action('init', array(&$this, 'fe_init'));
			add_action( 'admin_init', array(&$this, 'admin_init') );
			add_action( 'tracker_hourly_event_hook', array(&$this, 'tracker_do_this_hourly'));
			
			$this->tmp = ABSPATH .'wp-content/uploads/tmp';
			$this->launchpad = ABSPATH . 'wp-content/uploads/lp';
		}
		
		function admin_init(){
			$this->session();
		}
		
		function fe_init(){
			$this->session();
		}
			
		function install(){
			
		}
		
		function de_activate(){
			
		}
		
		function frontend_scripts(){
			$this->session();
		}
		
		function scripts_method(){
			wp_enqueue_script('jquery');
			wp_enqueue_script('jquery-ui-core');
			wp_enqueue_style( 'tw-bs_bootstrap', plugins_url('css/tw-bs.3.1.1.css', __FILE__) );
			wp_enqueue_script('bootstrap-js', plugins_url('js/bootstrap.min_.js', __FILE__));
			$this->session();
		}
		
		function admin_menu(){
			add_menu_page('Leak Tracker', 'Leak Tracker', 'manage_options', 'leak_tracker_decode_img', array(&$this, 'decode_jpg_img'));
			add_submenu_page('leak_tracker_decode_img','TMP Folder', 'TMP Folder', 'manage_options', 'leak_tracker_tmp', array(&$this,'tmp_folder'));
			add_submenu_page('leak_tracker_decode_img','Tracker Settings', 'Membership Leak Tracker Settings', 'manage_options', 'leak_tracker_settings', array(&$this,'setting_page'));
		}
		
		function tracker_do_this_hourly(){
			$pth = $this->tmp .'/*';
			$n =0;
			foreach (glob($pth) as $filename) {
				$n++;
				if($n > 30) break;
				$this->recursive_remove_via_cron($filename);
			}
			
		}
		function recursive_remove_via_cron($dir) {
			$structure = glob(rtrim($dir, "/").'/*');
			$settings = get_option('aspk_stegano_settings');
			$time_diff = intval($settings['delete_time']) * 3600;
			if (is_array($structure)) {
				foreach($structure as $file) {
					if (is_dir($file)){
						$this->recursive_remove_via_cron($file);
					}elseif (is_file($file)){
						$mt = filemtime($file);
						if( (time() - $mt) >  $time_diff) unlink($file);
						
					}
				}
			}
			rmdir($dir);
		}
		
		
		
		function tmp_folder(){
			echo '<div style="min-height:500px;">';
			if(! is_dir($this->tmp)){
				if(! mkdir($this->tmp)){
					echo '<div class="error">Cannot Create tmp Folder</div>';
					return;
				}
			}
			if(isset($_POST['aspk_del_tmp_folder'])){
				echo '<div style="margin-bottom:2em;"><h2>Pruging.....</h2></div>';
				
				$pth = $this->tmp .'/*';
				foreach (glob($pth) as $filename) {
					$this->recursive_remove($filename);
				}
			}
			
			
			$size = $this->get_dir_size($this->tmp);
			?>
				
				<div style="margin-bottom:3em;"><h2>CURRENT CONTENTS IN TMP FOLDER</h2><div>
				<div style="margin-bottom:2em;">tmp foder size: <?php echo $size;?> MB</div>
				<div style="margin-bottom:2em;"><h3>LIST of Folders</h3></div>
			<?php
			$pth = $this->tmp .'/*';
			
			foreach (glob($pth) as $filename) {
				$fn = str_replace($this->tmp .'/','',$filename);
				echo '<div>'.$fn.'</div>';
			}
			?>
				<form name="dc" method="post" action="">
					<div style="margin-top:2em;" ><input class="btn button-primary" type="submit" name="aspk_del_tmp_folder" value="Purge TMP Folder"></div>
				</form>
				</div>
			<?php
			
		}
		
		function recursive_remove($dir) {
			$structure = glob(rtrim($dir, "/").'/*');
			if (is_array($structure)) {
				foreach($structure as $file) {
					if (is_dir($file)){
						$this->recursive_remove($file);
					}elseif (is_file($file)){
						echo '<div>Purging '.$file.'</div>';
						unlink($file);
					}
				}
			}
			rmdir($dir);
		}
		
		function get_dir_size($directory) {
			$size = 0;
			foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($directory)) as $file) {
				$size += $file->getSize();
			}
			return round(($size/1024)/1024);
		}
		
		function decode_img($pth){
			//$pth fullpath and name
			
			if(empty($pth)) return false;
			
			$info = $this->get_image_info($pth);
			
			if(isset($info['has_info'])){
				if($info['has_info'] == '4298') return $info;
			}
			
			return false;
		}
		
		function show_info($info, $fname){
			$settings = get_option('aspk_stegano_settings');
			
			?><div style="clear:both;padding:1em;background-color:white;max-width:50em;">
				<div style="clear:both;margin-bottom:1em;"><h2>Trying to Decode <?php echo $fname;?></h2></div>
				<div style="clear:both;margin-bottom:1em;">
					<?php if(isset($info['username'])){ ?>
						<div style="clear:both;"><b>UserName:</b></div>
						<div style="clear:both;"><?php
							if(! empty($settings['info_password'])){
								echo $this->decrypt($info['username'], $settings['info_password']);
							}else{
								echo $info['username'];
							}
						
						?></div>
					<?php } //username ?>
				</div>
				
				<div style="clear:both;margin-bottom:1em;">
					<?php if(isset($info['current_date'])){ ?>
						<div style="clear:both;"><b>Date:</b></div>
						<div style="clear:both;"><?php echo $info['current_date'];?></div>
					<?php } //current_date ?>
				</div>
				
				<div style="clear:both;margin-bottom:1em;">
					<?php if(isset($info['source_ip'])){ ?>
						<div style="clear:both;"><b>Source IP Address:</b></div>
						<div style="clear:both;"><?php echo $info['source_ip'];?></div>
					<?php } //source_ip ?>
				</div>
				
				<div style="clear:both;margin-bottom:1em;">
					<?php if(isset($info['proxy_ip'])){ ?>
						<div style="clear:both;"><b>Proxy IP Address:</b></div>
						<div style="clear:both;"><?php echo $info['proxy_ip'];?></div>
					<?php } //proxy_ip ?>
				</div>
				
				<div style="clear:both;margin-bottom:1em;">
					<?php if(isset($info['device_used'])){ ?>
						<div style="clear:both;"><b>Device Used:</b></div>
						<div style="clear:both;"><?php echo $info['device_used'];?></div>
					<?php } //device_used ?>
				</div>
				
				<div style="clear:both;margin-bottom:1em;">
					<?php if(isset($info['browser_used'])){ ?>
						<div style="clear:both;"><b>Browser Used:</b></div>
						<div style="clear:both;"><?php echo $info['browser_used'];?></div>
					<?php } //browser_used ?>
				</div>
				
				<div style="clear:both;margin-bottom:1em;">
					<?php if(isset($info['operating_system'])){ ?>
						<div style="clear:both;"><b>Operating System:</b></div>
						<div style="clear:both;"><?php echo $info['operating_system'];?></div>
					<?php } //operating_system ?>
				</div>
				
				<div style="clear:both;margin-bottom:1em;">
					<?php if(isset($info['web_url_request'])){ ?>
						<div style="clear:both;"><b>Web URL Request:</b></div>
						<div style="clear:both;"><?php
							if(! empty($settings['info_password'])){
								echo $this->decrypt($info['web_url_request'], $settings['info_password']);
							}else{
								echo $info['web_url_request'];
							}
						
						?></div>
					<?php } //web_url_request ?>
				</div>
				
				<div style="clear:both;margin-bottom:1em;">
					<?php if(isset($info['full_path'])){ ?>
						<div style="clear:both;"><b>Full Path:</b></div>
						<div style="clear:both;"><?php
							if(! empty($settings['info_password'])){
								echo $this->decrypt($info['full_path'], $settings['info_password']);
							}else{
								echo $info['full_path'];
							}
						
						?></div>
					<?php } //full_path ?>
				</div>
			</div><!--info div white-->	
			<div style="clear:both;margin-top:2em;">&nbsp;</div>
			<?php
		}
		
		function decode_jpg_img(){
			?><div style="margin-bottom:3em;"><h2>Decode INFO from File</h2></div><?php
			
			if(isset($_POST['aspk_stegno_jpg'])){
				$upload = wp_upload_bits($_FILES["jpgfile"]["name"], null, file_get_contents($_FILES["jpgfile"]["tmp_name"]));
				
				
				if(! empty($upload['error'])){
					echo '<div class="error" style="margin-bottom:1em;">Upload Error:'.$upload['error'].'</div>';
				}else{
					$info = $this->decode_img($upload['file']);
					if(! $info){
						echo '<div class="error" style="margin-bottom:1em;">No Tracking Data Available on '.basename($upload['file']).'</div>';
						
					}else{
						$this->show_info($info, basename($upload['file']));
						
					}
				}
			}
			
			?>
				
				<div style="clear:both;background-color:white;padding: 1em;max-width:40em;">
					<form action="" method="post" enctype="multipart/form-data"  >
						<div style="clear:both;">Select File to Upload for Decoding:</div>
						<div style="clear:both;"><input type="file" name="jpgfile" id="jpgfile" requried  ></div>
						<div style="clear:both;margin-top:3em;"><input class="btn button-primary" type="submit" value="Upload File" name="aspk_stegno_jpg"></div>
					</form>
				</div>
			<?php
		}
		
				
	
		
		
		function create_zip_from_folder($path, $files, $password = ""){
			// $path absolute path including zip file name and ext
				
			$zip = new ZipArchive();
			if(! empty($password)){
				$xinfo = pathinfo($path);
				$path = $xinfo['dirname'].'/'.$xinfo['filename'].time().'.'.$xinfo['extension'];
				$new_file = $xinfo['dirname'].'/'.$xinfo['filename'].'.'.$xinfo['extension'];
			}
			
			$zip_name = $path;
			
			if($zip->open($zip_name, ZIPARCHIVE::CREATE)!==TRUE) return false;
			 
			foreach($files as $file){
				$zip->addFile($file, basename($file));
			}
			 
			$zip->close();
			if(! empty($password)){
				$cmd = "zip -j -P  ".escapeshellarg($password)."  ". escapeshellarg($new_file)."  ". escapeshellarg($zip_name). " > /dev/null; rm ".escapeshellarg($path)." >/dev/null" ;
				
				@system($cmd);
				
			}
			
			return true;	
		}
		
		function session(){
		
		   if(!session_id()){
				session_start();
			}
		}
		
		function leaktracker($atts){
		
			$para = shortcode_atts( array(
			 'name' => 'Download.zip',
			 'uploadspath' => '',
			 'fullpath' => '',
			 'pattern' => '*.*',
			 'password' => '',
			 ), $atts );
			
			$settings = get_option('aspk_stegano_settings', NULL);
			
			if(! $settings){
				return "Please Save Settings before use";
			}
			
			if(! empty($para['fullpath']) && ! empty($para['uploadspath'])){
				return "Please use either fullpath or uploadspath but not both";
			}
			
			if(empty($para['fullpath']) && empty($para['uploadspath'])){
				return "Please set fullpath or uploadspath";
			}
			
			if(! empty($para['fullpath'])){ 
				$pth = $this->normalize_full_path($para['fullpath']);
			}else{
				$pth = $this->normalize_path($para['uploadspath']);
			}
			
			if(! is_dir($pth)){
				return "Path is Invalid ".$pth;
			}
			
			if(strpos($para['pattern'], '.') === false){
				return "Invalid Pattern ".$para['pattern'];
			}
			
			if (! is_user_logged_in() ) {
				return "Please Login to Download Files";
			}
			
			if(isset($_SESSION['aspk_stegno_path'])){
				$sess_pth = $_SESSION['aspk_stegno_path'];
				$url = $_SESSION['aspk_stegno_url'];
				
				if($sess_pth == $pth){
					if($settings['display_btn'] == 'on'){
						$img_src = plugins_url('img/download.png', __FILE__);
						$ret ='<script>
						function stegano_download(){
						window.location.href="'.$url.'";
						</script><input onclick="stegano_download()" type="image" src="'.$img_src.'" alt="Download Zip" >';
						return $ret;
					}else{
						$link_text = $settings['link_text'];
						if(empty($link_text)) $link_text = "Download Zip";
						
						return '<div><a href="'.$url.'">'.$link_text.'</a></div>';
					}
				}
			}
			
			$file_list = $this->get_file_list($pth, $para['pattern']);
			
			if(empty($file_list)){
				return "No Files Found";
			}
			
			$filter_files = array();
			foreach($file_list as $item){
				$x = pathinfo($item);
				if(substr($x['filename'],-4) == '_sml') continue;
				$filter_files[] = $item;
			}
			
			
			$pick_list = $this->get_pick_list($filter_files);
			
			if(! is_dir($this->tmp)){
				if(! mkdir($this->tmp)) return "Cannot Create tmp Folder ".$this->tmp;
			}
			
			$src_folder = $this->prepare_tmp_folder($pick_list);
			
			if(! $src_folder) return "Cannot prepare src folder";
			
			$src_folder_tmp = $src_folder .'/tmp';
			
			$files = $this->get_file_list($src_folder_tmp, $para['pattern']);
			
			$info = $this->prepare_info($pth);
			$info = serialize($info);
			
			
			foreach($files as $src_image){
				$target_image = $src_folder .'/'. basename($src_image);
				$ret = $this->create_stegano_image($src_image, $target_image, $info);
				
				if(! $ret) return "Create Leak Track Failed";
				unlink($src_image);
			}
			
			$files = $this->get_file_list($src_folder, $para['pattern']);
			
			$name = str_ireplace('.zip','',$para['name']) . '.zip';
			$zip_file = $src_folder .'/'.$name;
			
			$ret  = $this->create_zip_from_folder($zip_file, $files, $para['password']);
			if(! $ret) return "Failed to create zip";
			
			
			$url = esc_url(home_url('/')) . strstr($zip_file, 'wp-content');
			
			
			if($settings['display_button_link'] == 'on'){
				$img_src = plugins_url('img/download.png', __FILE__);
				$ret ='<script>
				function stegano_download(){
				window.location.href="'.$url.'";
				}
				</script><input onclick="stegano_download()" type="image" src="'.$img_src.'" alt="Download Zip" >';
				return $ret;
			}else{
				$link_text = $settings['display_text_link'];
				if(empty($link_text)) $link_text = "Download Zip";
				
				return '<div><a href="'.$url.'">'.$link_text.'</a></div>';
			}
			
			if(session_id()){
				$_SESSION['aspk_stegno_path'] = $pth;
				$_SESSION['aspk_stegno_url'] = $url;
			}
		}
		
		
		
		function prepare_tmp_folder(&$pick_list){
			//one dim array with full path of file
			$new_folder = wp_unique_filename( $this->tmp, 'working_storage' );
			$new_folder = $this->tmp .'/'.$new_folder; 
			
			if(! mkdir($new_folder)) return false;
			if(! mkdir($new_folder.'/tmp/')) return false;
			$ret = file_put_contents($new_folder .'/index.html','Why we are here?');
			if(! $ret) return false;
			
			foreach($pick_list as $item){
				$new_name = $new_folder .'/tmp/'. basename($item);
				if(! copy($item, $new_name)) return false;
			}
			return $new_folder;
		}
		
		
		function get_pick_list(&$file_list){
			//one dim array with full path file names
			
			sort($file_list);
			
			$pick_list = array();
			$last_item = "";
			$last_size = 0;
			$size_item = "";
			
			foreach($file_list as $item){
				if(empty($last_item)){
					$last_item = $item;
					$last_size = filesize($item);
					$size_item = $item;
				}else{
					similar_text($this->sanitize_item($last_item), $this->sanitize_item($item), $percent);
					
					if($percent > 97){ //same series
						$size = filesize($item);
						if($size > $last_size){
							$last_size = $size;
							$size_item = $item;
						}
					}else{
						$pick_list[] = $size_item;
						
						$last_size = filesize($item);
						$size_item = $item;
					}
					$last_item = $item;
				}
			}

			$pick_list[] = $size_item;
			return $pick_list;
			
		}
		
		function sanitize_item($item){
			$f = basename($item);
			$y = strrpos($f, "-");
			$z = substr($f, $y);
			if(strstr($z, 'x') === false){
				$m = pathinfo($f);
				return $m['filename'];
			}
			$r = strstr($f, $z, true);
			return $r;
		}
		
		
		function get_file_list($pth, $pattern){
			$lst = array();
			$pth = $pth .'/'. $pattern;
			
			foreach (glob($pth) as $filename) {
				$lst[] = $filename;
			}
			return $lst;
		}
		
		
		// should be return real path
		function normalize_full_path($pth){
			//$pth is uploads path
			$pth = rtrim( $pth,'/');
			$pth = ltrim( $pth,'/');
			
			$pth = '/'.$pth.'/';
			
			return $pth;
		}
		
		function normalize_path($pth){
			//$pth is full path
			$pth = rtrim( $pth,'/');
			$pth = ltrim( $pth,'/');
			
			$pth = ABSPATH .'wp-content/uploads/'. $pth;
			
			return $pth;
		}
		
		function prepare_info($src_folder){ //full path
			$info = array();
			$settings = get_option('aspk_stegano_settings');
			$info['username'] = "";
			$info['current_date'] = "";
			$info['source_ip'] = "";
			$info['proxy_ip'] = "";
			$info['device_used'] = "";
			$info['browser_used'] = "";
			$info['operating_system'] = "";
			$info['web_url_request'] = "";
			$info['full_path'] = "";
			
			if($settings['stegano_name'] == 'on'){
				$current_user = wp_get_current_user();
				if ( !($current_user instanceof WP_User) ){
					$info['username'] = 'guest';
				}else{
					$info['username'] = $current_user->user_login;
				}
				if(! empty($settings['info_password'])){
					$info['username'] = $this->encrypt($info['username'], $settings['info_password']);
				}
			}
			
			if($settings['current_date'] == 'on'){
				$info['current_date'] =  date('Y-m-d h:i:s');
				
			}
			
			if($settings['source_ip'] == 'on'){
				$info['source_ip'] =  $_SERVER['REMOTE_ADDR'];
			}
			
			if($settings['proxy_ip'] == 'on'){
				if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
					$info['source_ip'] = $_SERVER['HTTP_X_FORWARDED_FOR'];
					$info['proxy_ip'] = $_SERVER['REMOTE_ADDR'];
				}
			}
			
			$browser = $this->get_browser_info();

			if($settings['device_used'] == 'on'){
				$device = "Other";
				if($browser->isMobileDevice == '1' || $browser->isMobile == 1){
					$device = "Mobile Device";
				}elseif($browser->isTablet == '1'){
					$device = "Mobile Device";
				}elseif($browser->isComputer == '1'){
					$device = "Computer";
				}
				
				$info['device_used'] =  $device;
			}
			
			if($settings['browser_used'] == 'on'){
				$info['browser_used'] =  $browser->browserFull;
			}
			
			if($settings['operating_system'] == 'on'){
				$info['operating_system'] =  $browser->osFull;
			}
			
			if($settings['web_url_request'] == 'on'){
				if(! empty($settings['info_password'])){
					$info['web_url_request'] =  $this->encrypt($this->get_current_url(), $settings['info_password']);
				}else{
					$info['web_url_request'] =  $this->get_current_url();
				}
			}
			
			if($settings['full_path'] == 'on'){

				if(! empty($settings['info_password'])){
					$info['full_path'] =  $this->encrypt($src_folder, $settings['info_password']);
				}else{
					$info['full_path'] =  $src_folder;
				}
			}
			
			$info['has_info'] = '4298';
			
			return $info;
		}
		
		function get_current_url($full = true) {
			if (isset($_SERVER['REQUEST_URI'])) {
				$parse = parse_url(
					(isset($_SERVER['HTTPS']) && strcasecmp($_SERVER['HTTPS'], 'off') ? 'https://' : 'http://') .
					(isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : (isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : '')) . (($full) ? $_SERVER['REQUEST_URI'] : null)
				);
				$parse['port'] = $_SERVER["SERVER_PORT"]; // Setup protocol for sure (80 is default)
				
				return $parse['scheme'].'://'.$parse['scheme'].$parse['host'].$parse['path'];  //':'.$parse['port'];
			}
		}
		
		function get_browser_info(){
			
			require_once __DIR__ .'/ua-parser-php/UAParser.php';
			
			$ua = $_SERVER['HTTP_USER_AGENT'];
			$result = UA::parse($ua);
			return $result;
		}
		
		function setting_page(){
			$msg = "";
			if(isset($_POST['aspk_stegano_setting'])){
				$aspk_uname = $_POST['u_name'];
				$aspk_cd = $_POST['cd_t'];
				$aspk_source_ip =$_POST['s_ip'];
				$aspk_proxy_ip= $_POST['p_ip'];
				$aspk_device_used = $_POST['d_used'];
				$aspk_browser_used =$_POST['b_used'];
				$aspk_o_system =$_POST['os'];
				$aspk_web_url =$_POST['web_url'];
				$aspk_full_path =$_POST['f_path'];
				
				$stegano_zip_arr = array();
				$stegano_zip_arr['stegano_name'] 	= $aspk_uname;
				$stegano_zip_arr['current_date']	= $aspk_cd;
				$stegano_zip_arr['source_ip'] 		= $aspk_source_ip;
				$stegano_zip_arr['proxy_ip']		= $aspk_proxy_ip;
				$stegano_zip_arr['device_used'] 	= $aspk_device_used;
				$stegano_zip_arr['browser_used'] 	= $aspk_browser_used;
				$stegano_zip_arr['operating_system']= $aspk_o_system;
				$stegano_zip_arr['web_url_request'] = $aspk_web_url;
				$stegano_zip_arr['full_path'] 		= $aspk_full_path;
				$stegano_zip_arr['info_password'] 		= $_POST['info_password'];
				$stegano_zip_arr['display_text_link'] 		= $_POST['display_text_link'];
				$stegano_zip_arr['display_button_link'] 		= $_POST['display_button_link'];
				$stegano_zip_arr['delete_time'] 		= $_POST['delete_time'];
				
				
				
				update_option('aspk_stegano_settings',$stegano_zip_arr);
				
				$timestamp = wp_next_scheduled( 'tracker_hourly_event_hook' );
				wp_unschedule_event( $timestamp, 'tracker_hourly_event_hook');
				wp_schedule_event( time(), 'hourly', 'tracker_hourly_event_hook' );
				
				$msg="Settings have been Saved";
			}
				$get_stegano_settings = get_option('aspk_stegano_settings');
				 
		  ?>
		  <div class="tw-bs container" style = "background-color:white; width:50em; float:left; clear:left;margin-top:1em;"><!-- start container -->
			   <div style="clear:left;" class="row">
				 <div class="col-md-12" style="margin-top:1em;margin-left:1em;"><h2>Membership Leak Tracker Settings</h2></div>
			  </div>
			  <div style="clear:both;margin: 1em;"><?php echo $msg;?></div>
			  <form  method="post" action="" >
					<div  class="row" style="clear:left;" >
						<div  class="col-md-5" style="margin-top:1em;"></div>
						<div  class="col-md-1" style="margin-top:1em;margin-left:1em;"><b>ON</b></div>
						<div  class="col-md-1" style="margin-top:1em;"><b>OFF</b></div>
						<div  class="col-md-5" style="margin-top:1em;"></div>
					</div>
					<div  class="row" style="clear:left;">
						<div  class="col-md-5" style="margin-top: 1em; margin-left:1em;"><b>User Name</b></div>
						<div  class="col-md-1" style="margin-top:1em;">
							<input type = "radio" <?php if (($get_stegano_settings['stegano_name'] == 'on') || (! isset($get_stegano_settings['stegano_name']))){ echo checked ; } ?> name = "u_name" value = "on" />
						</div>
						<div  class="col-md-1" style="margin-top:1em;">
							<input type = "radio" <?php if($get_stegano_settings['stegano_name'] == 'off'){ echo checked ; } ?> name = "u_name" value = "off" />
						</div>
						<div  class="col-md-5" style="margin-top:1em;"></div>
					</div>
					<div  class="row" style="clear:left;">
						<div  class="col-md-5" style="margin-top: 1em; margin-left:1em;"><b>Current Date & Time</b></div>
						<div  class="col-md-1" style="margin-top:1em;">
							<input type = "radio" <?php if (($get_stegano_settings['current_date'] == 'on') || (! isset($get_stegano_settings['current_date']))){ echo checked ; }?> checked name = "cd_t" value = "on" />
						</div>
						<div  class="col-md-1" style="margin-top:1em;">
							<input type = "radio"  <?php if($get_stegano_settings['current_date'] == 'off'){ echo checked ; } ?> name = "cd_t" value = "off" />
						</div>
						<div  class="col-md-5" style="margin-top:1em;" ></div>
					</div>
					<div  class="row" style="clear:left;">
						<div  class="col-md-5" style="margin-top: 1em; margin-left:1em;"><b>Source IP Address</b></div>
						<div  class="col-md-1" style="margin-top:1em;">
							<input type = "radio" <?php if (($get_stegano_settings['source_ip'] == 'on') || (! isset($get_stegano_settings['source_ip']))){ echo checked ; }?> checked name = "s_ip" value = "on" />
						</div>
						<div  class="col-md-1" style="margin-top:1em;">
							<input type = "radio"  <?php if($get_stegano_settings['source_ip'] == 'off'){ echo checked ; } ?> name = "s_ip" value = "off" />
						</div>
						<div  class="col-md-5" style="margin-top:1em;" ></div>
					</div>
					<div  class="row" style="clear:left;">
						<div  class="col-md-5" style="margin-top: 1em; margin-left:1em;"><b>Proxy IP Address</b></div>
						<div  class="col-md-1" style="margin-top:1em;">
							<input type = "radio"  <?php if (($get_stegano_settings['proxy_ip'] == 'on') || (! isset($get_stegano_settings['proxy_ip']))){ echo checked ; }?> checked name = "p_ip" value = "on" />
						</div>
						<div  class="col-md-1" style="margin-top:1em;">
							<input type = "radio" <?php if($get_stegano_settings['proxy_ip'] == 'off'){ echo checked ; } ?> name = "p_ip" value = "off" />
						</div>
						<div  class="col-md-5" style="margin-top:1em;" ></div>
					</div>
					<div  class="row" style="clear:left;">
						<div  class="col-md-5" style="margin-top: 1em; margin-left:1em;"><b>Device Used</b></div>
						<div  class="col-md-1" style="margin-top:1em;">
							<input type = "radio"  <?php if (($get_stegano_settings['device_used'] == 'on') || (! isset($get_stegano_settings['device_used']))){ echo checked ; }?> name = "d_used" value = "on" />
						</div>
						<div  class="col-md-1" style="margin-top:1em;">
							<input type = "radio" <?php if($get_stegano_settings['device_used'] == 'off'){ echo checked ; } ?> name = "d_used" value = "off" />
						</div>
						<div  class="col-md-5" style="margin-top:1em;" ></div>
					</div>
					<div  class="row" style="clear:left;">
						<div  class="col-md-5" style="margin-top: 1em; margin-left:1em;"><b>Browser Used</b></div>
						<div  class="col-md-1" style="margin-top:1em;">
							<input type = "radio"  <?php if (($get_stegano_settings['browser_used'] == 'on') || (! isset($get_stegano_settings['browser_used']))){ echo checked ; }?> name = "b_used" value = "on" />
						</div>
						<div  class="col-md-1" style="margin-top:1em;">
							<input type = "radio" <?php if($get_stegano_settings['browser_used'] == 'off'){ echo checked ; } ?> name = "b_used" value = "off" />
						</div>
						<div  class="col-md-5" style="margin-top:1em;" ></div>
					</div>
					<div  class="row" style="clear:left;">
						<div  class="col-md-5" style="margin-top: 1em; margin-left:1em;"><b>Operating System</b></div>
						<div  class="col-md-1" style="margin-top:1em;">
							<input type = "radio"  <?php if (($get_stegano_settings['operating_system'] == 'on') || (! isset($get_stegano_settings['operating_system']))){ echo checked ; }?> name = "os" value = "on" />
						</div>
						<div  class="col-md-1" style="margin-top:1em;">
							<input type = "radio" <?php if($get_stegano_settings['operating_system'] == 'off'){ echo checked ; } ?> name = "os" value = "off" />
						</div>
						<div  class="col-md-5" style="margin-top:1em;" ></div>
					</div>
					<div  class="row" style="clear:left;">
						<div  class="col-md-5" style="margin-top: 1em; margin-left:1em;"><b>Web URL of Request Made for the Zip File</b></div>
						<div  class="col-md-1" style="margin-top:1em;">
							<input type = "radio"  <?php if (($get_stegano_settings['web_url_request'] == 'on') || (! isset($get_stegano_settings['web_url_request']))){ echo checked ; }?>  name = "web_url" value = "on" />
						</div>
						<div  class="col-md-1" style="margin-top:1em;">
							<input type = "radio"<?php if($get_stegano_settings['web_url_request'] == 'off'){ echo checked ; } ?> name = "web_url" value = "off" />
						</div>
						<div  class="col-md-5" style="margin-top:1em;" ></div>
					</div>
					<div  class="row" style="clear:left;">
						<div  class="col-md-5" style="margin-top: 1em; margin-left:1em;"><b>Full Path to Source Folder</b></div>
						<div  class="col-md-1" style="margin-top:1em;">
							<input type = "radio"  <?php if (($get_stegano_settings['full_path'] == 'on') || (! isset($get_stegano_settings['full_path']))){ echo checked ; }?> name = "f_path" value = "on" />
						</div>
						<div  class="col-md-1" style="margin-top:1em;">
							<input type = "radio" <?php if($get_stegano_settings['full_path'] == 'off'){ echo checked ; } ?> name = "f_path" value = "off" />
						</div>
						<div  class="col-md-5" style="margin-top:1em;" ></div>
					</div>
					
					<div  class="row" style="clear:left;">
						<div  class="col-md-5" style="margin-top: 1em; margin-left:1em;"><b>Encryption Password</b></div>
						<div  class="col-md-1" style="margin-top:1em;">
							<input type = "text"  value="<?php if (isset($get_stegano_settings['info_password'])){ echo $get_stegano_settings['info_password'] ; }?>" name = "info_password" />
						</div>
						<div  class="col-md-5" style="margin-top:1em;" ></div>
					</div>
					
					<div  class="row" style="clear:left;">
						<div  class="col-md-5" style="margin-top: 1em; margin-left:1em;"><b>Display Text Link</b></div>
						<div  class="col-md-1" style="margin-top:1em;">
							<input type = "text"  value="<?php if (isset($get_stegano_settings['display_text_link'])){ echo $get_stegano_settings['display_text_link'] ; }else{echo "Download Zip";}?>" name = "display_text_link" />
						</div>
						<div  class="col-md-5" style="margin-top:1em;" ></div>
					</div>
					
					<div  class="row" style="clear:left;">
						<div  class="col-md-5" style="margin-top: 1em; margin-left:1em;"><b>Display Button Link Instead of Text</b></div>
						<div  class="col-md-1" style="margin-top:1em;">
							<input type = "radio"  <?php if ($get_stegano_settings['display_button_link'] == 'on'){ echo checked ; }?> name = "display_button_link" value = "on" />
						</div>
						<div  class="col-md-1" style="margin-top:1em;">
							<input type = "radio" <?php if(($get_stegano_settings['display_button_link'] == 'off') || (! isset($get_stegano_settings['display_button_link']))){ echo checked ; } ?> name = "display_button_link" value = "off" />
						</div>
						<div  class="col-md-5" style="margin-top:1em;" ></div>
					</div>
					
					<div  class="row" style="clear:left;">
						<div  class="col-md-5" style="margin-top: 1em; margin-left:1em;"><b>Keep The ZIP File & TMP Data for at Least(minutes)</b></div>
						<div  class="col-md-1" style="margin-top:1em;">
							<input type = "text"  value="<?php if (isset($get_stegano_settings['delete_time'])){ echo $get_stegano_settings['delete_time'] ; }else{echo "10";}?>" name = "delete_time" />
						</div>
						<div  class="col-md-5" style="margin-top:1em;" ></div>
					</div>
					
					<div  class="row" style="clear:left;">
						<div style="margin-top: 1em; margin-left:1em;" class="col-md-1">
							<input class = "button-primary" type="submit" value="Save Settings" name="aspk_stegano_setting">
						</div>
						<div style="margin-top:1em;" class="col-md-11"></div>
					</div>
					
				</form>
			</div><!-- end container -->
		  
		<?php
	    }
		
		function create_stegano_image($src_image, $target_image, $info){
			//src and tgt are full paths.  $info is serialized
			$pk = $info;
			$len = strlen($pk);
			$sz = serialize(sprintf('%03d', $len));

			if($len < 989){
				$fill = 989 - $len;
			}else{
				$fill = 0;
			}
			$oput = $sz . $pk . str_repeat(".", $fill);
			try{
				$inbytes = file_get_contents($src_image);
				file_put_contents($target_image,$inbytes.$oput);
			}catch(Exception $e){
				$this->logmsg($e->getMessage());
				return false;
			}
			return true;
		}
		
		
		
		
		function get_image_info($image){
			//$image is full path to image under interogation
			
			try{
				$fp = fopen($image, 'r');
			}catch(Exception $e){
				
				$this->logmsg($e->getMessage());
				return false;
			}
			fseek($fp, -999, SEEK_END); // It needs to be negative
			$len = fgets($fp, 10);
			$info_len = unserialize($len);
			fseek($fp, -989, SEEK_END);
			$data = fgets($fp, $info_len+1);
			
			$ret = unserialize($data);
			
			return $ret;

		}
		
		function encrypt($message, $key){
			$encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $message, MCRYPT_MODE_CBC, md5(md5($key))));
			return $encrypted;
		}
		
		function decrypt($message, $key){
			$decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($message), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
			return $decrypted;
		}
		
		function logmsg($msg, $tp = "Error:"){
		
			if($this->enable_debug){
				$f = __DIR__ . '/log/message.log'; 
				$msg = date('Y-m-d h:i:s').' '.$tp.' '.$msg.PHP_EOL;
				
				file_put_contents($f, $msg, FILE_APPEND);
			}
		}
		
			
		
	}//class ends
}//existing class ends

new Membership_Leak_Tracker();